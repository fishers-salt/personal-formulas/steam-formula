# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_pacman_repo = tplroot ~ '.pacman.repo' %}
{%- from tplroot ~ "/map.jinja" import mapdata as steam with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_pacman_repo }}

steam-package-install-pkgs-installed:
  pkg.installed:
    - pkgs: {{ steam.pkgs }}

{%- set gpus = salt.grains.get('gpus', []) %}
{%- for gpu in gpus %}
{%- set pkgs = steam.drivers.get(gpu.vendor, []) %}
steam-package-install-{{ gpu.vendor }}-gpu-driver-installed:
  pkg.installed:
    - pkgs: {{ pkgs }}
    - require:
      - sls: {{ sls_pacman_repo }}
{%- endfor %}
