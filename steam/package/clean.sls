# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as steam with context %}

steam-package-clean-pkgs-removed:
  pkg.removed:
    - pkgs: {{ steam.pkgs }}

{%- set gpus = salt.grains.get('gpus', []) %}
{%- for gpu in gpus %}
{%- set all_pkgs = steam.drivers.get(gpu.vendor, []) %}
{# only remove 32bit packages #}
{%- set pkgs = [] %}
{%- for pkg in all_pkgs %}
{%- do pkgs.append(pkg) if pkg.startswith('lib32') %}
{%- endfor %}
{#
steam-package-clean-{{ gpu.vendor }}-gpu-driver-removed:
  pkg.removed:
    - pkgs: {{ pkgs }}
#}
{%- endfor %}
