# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as steam with context %}

steam-pacman-clean-config-option-removed:
  ini.options_absent:
    - name: /etc/pacman.conf
    - sections:
        multilib:
          - Include

steam-pacman-clean-config-section-removed:
  ini.sections_absent:
    - name: /etc/pacman.conf
    - sections:
        - multilib
    - require:
      - steam-pacman-clean-config-option-removed
