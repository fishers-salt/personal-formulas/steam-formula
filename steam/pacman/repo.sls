# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as steam with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

steam-pacman-repo-config-managed:
  ini.options_present:
    - name: /etc/pacman.conf
    - sections:
        multilib:
          Include: /etc/pacman.d/mirrorlist

steam-pacman-repo-pacman-updated:
  cmd.run:
    - name: pacman --sync --refresh
    - onchanges:
      - steam-pacman-repo-config-managed
