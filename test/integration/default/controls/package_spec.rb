# frozen_string_literal: true

packages = %w[steam ttf-liberation]

packages.each do |pkg|
  control "steam-package-install-pkgs-#{pkg}-installed" do
    title 'should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end

packages = %w[lib32-mesa lib32-vulkan-intel mesa vulkan-intel]
packages.each do |pkg|
  control "steam-package-install-intel-gpu-driver-#{pkg}-installed" do
    title 'should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end
