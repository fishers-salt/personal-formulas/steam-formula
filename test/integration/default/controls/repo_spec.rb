# frozen_string_literal: true

control 'steam-pacman-repo-config-managed' do
  title 'should match desired lines'

  describe file('/etc/pacman.conf') do
    its('content') { should include('[multilib]') }
    its('content') do
      should match(%r{\[multilib\]\nInclude = /etc/pacman.d/mirrorlist})
    end
  end
end
