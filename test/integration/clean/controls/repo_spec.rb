# frozen_string_literal: true

control 'steam-pacman-clean-config-absent' do
  title 'should not include section'

  describe file('/etc/pacman.conf') do
    its('content') { should_not include('[multilib]') }
    its('content') do
      should_not match(%r{\[multilib\]\nInclude = /etc/pacman.d/mirrorlist})
    end
  end
end
