# frozen_string_literal: true

packages = %w[steam ttf-liberation]

packages.each do |pkg|
  control "steam-package-clean-pkgs-#{pkg}-removed" do
    title 'should not be installed'

    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end

# packages = %w[lib32-mesa lib32-vulkan-intel]
# packages.each do |pkg|
#   control "steam-package-clean-intel-gpu-driver-#{pkg}-removed" do
#     title 'should not be installed'

#     describe package(pkg) do
#       it { should_not be_installed }
#     end
#   end
# end

# packages = %w[mesa vulkan-intel]
# packages.each do |pkg|
#   control "steam-package-clean-intel-gpu-driver-#{pkg}-remaining" do
#     title 'should still be installed'

#     describe package(pkg) do
#       it { should be_installed }
#     end
#   end
# end
